
CREATE TABLE COMMENT
(
  id BIGINT NOT NULL AUTO_INCREMENT ,
  author_id BIGINT,
  content VARCHAR(4096),
  index DECIMAL(38,28),
  time_stamp TIMESTAMP,
  path_string VARCHAR(4096),
  depth INT,
  parent_id BIGINT,
  sibling_to_id BIGINT,
  sibling_from_id BIGINT,
  PRIMARY KEY (id)
);
CREATE INDEX IDX_MYIDX1 ON COMMENT(index);
