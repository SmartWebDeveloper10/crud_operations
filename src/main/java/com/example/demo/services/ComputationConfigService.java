package com.example.demo.services;

import java.math.BigDecimal;
import java.math.MathContext;

public interface ComputationConfigService {
    public BigDecimal giveOne();
    public BigDecimal giveHalf();
    public MathContext giveMathContext();
    BigDecimal giveIndexEps();
    BigDecimal giveDbEps();
}
