package com.example.demo.services;

import com.example.demo.dto.CommentDto;
import com.example.demo.entity.Comment;

import java.util.Collection;
import java.util.List;

public interface CommentFilterService {
    public CommentDto filterComments(Collection<Comment> collection, Comment parent);
    public long giveTotalSize(CommentDto com);
    public List<Comment> filterCommentsForDelete(Collection<Comment> collection, Comment parent);
}
