package com.example.demo.services.impl;

import com.example.demo.dto.CommentDto;
import com.example.demo.entity.Comment;
import com.example.demo.services.CommentFilterService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.SessionScope;

import java.util.*;

@Service
@SessionScope
public class CommentFilterServiceImpl implements CommentFilterService {

    @Value("${comment.crud.scale}")
    private int SCALE;

    private class SortByDepth implements Comparator<Comment> {
        @Override
        public int compare(Comment a, Comment b) {
            if(a.getDepth()>b.getDepth()){
                return 1;
            }
            else if(a.getDepth()<b.getDepth()){
                return -1;
            }
            else{
                return 0;
            }
        }
    }

    private class DepthPair {
        private int depth;
        private int counter;

        public DepthPair(int depth){
            this.depth = depth;
            counter = 1;
        }
        public void increment(){
            this.counter++;
        }
        public int getDepth(){
            return this.depth;
        }
        public int getCounter(){
            return this.counter;
        }
    }

    public int checkDepthQueue(Deque<DepthPair> depthPairsQ){
        if(depthPairsQ.size()>2){
            return depthPairsQ.poll().getCounter();
        }
        else{
            return 0;
        }
    }
    public void updateDepthQueue(Deque<DepthPair> depthPairsQ, Comment comment){
        if(depthPairsQ.isEmpty()){
            depthPairsQ.addLast(new DepthPair(comment.getDepth()));
            return;
        }
        else if(depthPairsQ.peekLast().getDepth()== comment.getDepth()){
            depthPairsQ.peekLast().increment();
            return;
        }
        else{
            depthPairsQ.addLast(new DepthPair(comment.getDepth()));
            return;
        }
    }
/*
The filtering algorithm is a BFS algorithm.
The algorithm uses 1 auxiliary queue "commentQ" and 1 auxiliary deque "depthPairsQ"

 */
    @Override
    public CommentDto filterComments(Collection<Comment> collection, Comment parent) {
        if(collection.isEmpty()){
            return null;
        }
        List<Comment> collectionL = (List) collection;
        //First, we sort the input over depth
        Collections.sort(collectionL,new SortByDepth());
        Queue<CommentDto> commentQ = new LinkedList<>();
        Deque<DepthPair> depthPairsQ = new LinkedList<>();
        CommentDto parentDto = new CommentDto(parent);
        commentQ.offer(parentDto);

        //This variable hold the info if the parent is in the collection;
        //Recall, that in our index algorithm,
        // the parent is GUARANTEED to be within its bra and ket!
        boolean isParentThere = false;

        for(Comment tempCom : collectionL){
            CommentDto cdto = new CommentDto(tempCom);
            //   System.out.println("parent.id: "+parent.getId()+"; queue size is "+ commentQ.size()+"; deque size is "+depthPairsQ.size());
           //We check if the current tempCom is in the commentQ
            boolean hasParInQueue = hasParent(commentQ,cdto);
            if(tempCom.getDepth()<=parent.getDepth()){
                //If the parent is in the collection, we set the checker to true;
                if(tempCom.getId().equals(parent.getId())){
                    isParentThere = true;
                }
                continue;
            }//If there is no parent within depth<=parentDepth elements, then there is NO SUCH PARENT IN THE DB
            else if(isParentThere==false){
                return null;
            }
            else if( hasParInQueue ){
                //If the tempCom is in the commentQ, we update the  depth deque
                updateDepthQueue(depthPairsQ,tempCom);
                //...add the element to commentQ
                commentQ.offer(cdto);
                //...find the number of elements to remove from the commentQ
                int numberCommentsToRemove = checkDepthQueue(depthPairsQ);
                if(numberCommentsToRemove>0){
                    //...and remove them.
                    emptyQueueDto(commentQ,numberCommentsToRemove);
                }
                continue;
            }
        }
        return parentDto;
    }
    @Override
    public List<Comment> filterCommentsForDelete(Collection<Comment> collection, Comment parent) {
        List<Comment> result = new ArrayList<>();
        result.add(parent);
        if(collection.isEmpty()){
            return result;
        }
        List<Comment> collectionL = (List) collection;
        Collections.sort(collectionL,new SortByDepth());
        Queue<Comment> commentQ = new LinkedList<>();
        Deque<DepthPair> depthPairsQ = new LinkedList<>();
        boolean isParentThere = false;
        commentQ.offer(parent);

        for(Comment tempCom : collectionL){
            boolean hasParInQueue = hasParent(commentQ,tempCom);
            if(tempCom.getDepth()<=parent.getDepth()){
                if(tempCom.getId().equals(parent.getId())){
                    isParentThere = true;
                }
                continue;
            }
            else if(isParentThere==false){
                return new ArrayList<>();
            }
            else if( hasParInQueue){

                updateDepthQueue(depthPairsQ,tempCom);
                commentQ.offer(tempCom);
                result.add(tempCom);
                int numberCommentsToRemove = checkDepthQueue(depthPairsQ);
                if(numberCommentsToRemove>0){
                    emptyQueue(commentQ,numberCommentsToRemove);
                }
                continue;
            }
        }
        return result;
    }

    private boolean hasParent(Queue<CommentDto> Q, CommentDto q){
        for(CommentDto t: Q){
            if(q.getParent()==t.getId()){
                t.getChildren().add(q);
                return true;
            }
        }
        return false;
    }

    private boolean hasParent(Queue<Comment> Q, Comment q){
        for(Comment t: Q){
            if(q.getParent()==t.getId()){
               return true;
            }
        }
        return false;
    }

    private void emptyQueueDto(Queue<CommentDto> queue, int numberToRemove){
        if(numberToRemove == 0) {
            return;
        }
        for(int i=0;i<numberToRemove;i++){
            queue.remove();
        }
    }
    private void emptyQueue(Queue<Comment> queue, int numberToRemove){
        if(numberToRemove == 0) {
            return;
        }
        for(int i=0;i<numberToRemove;i++){
            queue.remove();
        }
    }

    public long giveTotalSize(CommentDto com){
        long counter = 0L;
        Queue<CommentDto> queue = new LinkedList<>();
        if(com!=null) {
            queue.add(com);
        }
        else{
            return 0;
        }
        while(!queue.isEmpty()){
            counter++;
            CommentDto current = queue.remove();

            if(current.getChildren().isEmpty()) {
                continue;
            }
            else{
                queue.addAll(current.getChildren());
                continue;
            }
        }
        return counter;
    }
}
