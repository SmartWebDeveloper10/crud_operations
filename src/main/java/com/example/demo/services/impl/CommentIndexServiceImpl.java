package com.example.demo.services.impl;

import com.example.demo.entity.Comment;
import com.example.demo.services.CommentIndexService;
import com.example.demo.services.ComputationConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.SessionScope;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

@Service
@SessionScope
public class CommentIndexServiceImpl implements CommentIndexService {

    @Value("${comment.crud.scale}")
    private int SCALE;
    @Value("${comment.crud.max2}")
    private int commentMax2;
    @Value("${comment.crud.max3}")
    private int commentMax3;

    private static MathContext mc;
    private static BigDecimal eps;
    private static BigDecimal one;
    private static BigDecimal half;



    @Autowired
    public CommentIndexServiceImpl(ComputationConfigService ccs){

        this.mc = ccs.giveMathContext();
        this.one = ccs.giveOne();
        this.half = ccs.giveHalf();
        this.eps = ccs.giveIndexEps();

    }


    public Comment setDepthOutOfPath(Comment c){
        if(c.getPathString()!=null) {
            List<String> path;
            path = Arrays.asList(c.getPathString().split("\\|"));
            c.setDepth(path.size());
        }
        else{
            c.setDepth(1);
        }
        return c;
    }

    public Comment setPathRnd(Comment c){
        int min2 = 1;
        int max2 = commentMax2;
        int min3 = 1;
        int max3 = commentMax3;
        int num;
        String path = c.getPathString();

        int depth = c.getDepth();
        Random random = new Random();
        if((depth==1 || path==null) && c.getId()!=null){
            c.setPathString(String.valueOf(c.getId()));
        }
        else if(depth==2 && path!=null){

            num = random.nextInt(max2 - min2) + min2;
            c.setPathString(path.concat("|").concat(String.valueOf(num)));

        }
        else if(depth>2 && path!=null){

            num = random.nextInt(max3 - min3) + min3;
            c.setPathString(path.concat("|").concat(String.valueOf(num)));
        }
        return c;
    }

    public Comment computeIndex(Comment c){

        List<BigDecimal> longPath = convertPathToBigDec(c.getPathString());

        c.setIndex(computeIndex(longPath));
        return c;

    }
    public BigDecimal computeNeighbourBra(Comment c){
        String pathString = c.getPathString();
        List<String> path;
        if(pathString!=null) {
            path = Arrays.asList(pathString.split("\\|"));
            if(path.size()==1){
                return new BigDecimal(path.get(0));

            }

            List<BigDecimal> longPath = convertPathToBigDec(path);
            return computeNeighbourBra(longPath);
        }
        else{
            return new BigDecimal(0);
        }

    }

    public BigDecimal computeNeighbourKet(Comment c){
        String pathString = c.getPathString();

        List<String> path;
        if(pathString!=null) {
            path = Arrays.asList(pathString.split("\\|"));
            if(path.size()==1){
                BigDecimal one = new BigDecimal(1);
                return new BigDecimal(path.get(0)).add(one,mc);

            }

            List<BigDecimal> longPath = convertPathToBigDec(path);
            return computeNeighbourKet(longPath);
        }
        else{
            return new BigDecimal(0);
        }

    }


    public BigDecimal computeIndex(List<BigDecimal> longPath){

        int pathLength = longPath.size();
        if(pathLength==1){
            return longPath.get(0).add(half,mc);
        }

        //We add 1 to the last element for the iterative procedure to be stable against rounding off errors
        BigDecimal tempInd = longPath.get(pathLength-1).add(half,mc);

        for(int i=pathLength-2;i>=0;i--){
            //  tempInd = longPath.get(i).add(one.divide(tempInd, MathContext.DECIMAL128));
            tempInd = longPath.get(i).add(one.divide(tempInd, mc),mc);
        }
        return tempInd;

    }
    public List<BigDecimal> convertPathToBigDec(String p){
        List<String> path = Arrays.asList(p.split("\\|"));
        List<BigDecimal> longPath = new ArrayList<>();
        for(String s:path) {
            BigDecimal bd = new BigDecimal(Long.valueOf(s).longValue());
            bd = bd.setScale(SCALE, RoundingMode.HALF_UP);
            longPath.add(bd);
        }
        return longPath;
    }
    public List<BigDecimal> convertPathToBigDec(List<String> path){

        List<BigDecimal> longPath = new ArrayList<>();
        for(String s:path) {
            BigDecimal bd = new BigDecimal(Long.valueOf(s).longValue());
            bd = bd.setScale(SCALE, RoundingMode.HALF_UP);
            longPath.add(bd);
        }
        return longPath;
    }
    public BigDecimal computeNeighbourBra(List<BigDecimal> longPath){
        int pathLength = longPath.size();
        if(pathLength==1){
            return longPath.get(0).add(one,mc);
        }

       // one = one.setScale(SCALE, RoundingMode.HALF_UP);
        BigDecimal tempInd = longPath.get(pathLength-1).add(one,mc);

        for(int i=pathLength-2;i>=0;i--){
           tempInd = longPath.get(i).add(one.divide(tempInd, mc),mc);
        }
        return tempInd;
    }
    public BigDecimal computeNeighbourKet(List<BigDecimal> longPath){

        int pathLength = longPath.size();
        if(pathLength==1){
            return longPath.get(0).add(eps,mc);
        }

        BigDecimal tempInd = longPath.get(pathLength-1).add(eps,mc);

        for(int i=pathLength-2;i>=0;i--){
            tempInd = longPath.get(i).add(one.divide(tempInd, mc),mc);
        }


        return tempInd;
    }


}
