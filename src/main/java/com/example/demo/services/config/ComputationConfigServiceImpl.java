package com.example.demo.services.config;

import com.example.demo.services.ComputationConfigService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

@Service
public class ComputationConfigServiceImpl implements ComputationConfigService {

    @Value("${comment.crud.scale}")
    private int SCALE;
    @Override
    public BigDecimal giveOne() {
        BigDecimal one = new BigDecimal(1);
        one = one.setScale(SCALE, RoundingMode.HALF_UP);
        return one;
    }

    @Override
    public BigDecimal giveHalf() {
        BigDecimal half = new BigDecimal("0.5");
        half = half.setScale(SCALE, RoundingMode.HALF_UP);
        return half;

    }

    @Override
    public MathContext giveMathContext() {
        return new MathContext(SCALE,RoundingMode.HALF_UP);
    }
    @Override
    public BigDecimal giveIndexEps() {
        BigDecimal eps = new BigDecimal("0.000000000000000000000000000000000000001");
        eps = eps.setScale(SCALE, RoundingMode.HALF_UP);
        return eps;
    }

    @Override
    public BigDecimal giveDbEps(){
        BigDecimal eps = new BigDecimal("0.000000000000000000000000001");
        eps = eps.setScale(SCALE, RoundingMode.HALF_UP);
        return eps;
    }
}
