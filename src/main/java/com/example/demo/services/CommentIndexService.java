package com.example.demo.services;

import com.example.demo.entity.Comment;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public interface CommentIndexService {

    public Comment setDepthOutOfPath(Comment c);

    public Comment setPathRnd(Comment c);

    public Comment computeIndex(Comment c);
    public BigDecimal computeNeighbourBra(Comment c);

    public BigDecimal computeNeighbourKet(Comment c);

    public BigDecimal computeIndex(List<BigDecimal> longPath);
    public List<BigDecimal> convertPathToBigDec(String p);
    public List<BigDecimal> convertPathToBigDec(List<String> path);
    public BigDecimal computeNeighbourBra(List<BigDecimal> longPath);
    public BigDecimal computeNeighbourKet(List<BigDecimal> longPath);
}
