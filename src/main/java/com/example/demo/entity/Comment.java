package com.example.demo.entity;


import com.example.demo.dto.CommentDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "COMMENT")
public class Comment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "content")
    private String content;

    @Column(name = "time_stamp")
    private Timestamp timestamp;

    @Column(name = "author_id")
    private long author;

    @Column(name = "sibling_from_id")
    private long siblingFrom;

    @Column(name = "sibling_to_id")
    private long siblingTo;

    @Column(name = "parent_id")
    private long parent;

    @Column(name = "index")
    private BigDecimal index;

    @Column(name = "path_string")
    private String pathString;

    @Column(name = "depth")
    private int depth = 1;
}