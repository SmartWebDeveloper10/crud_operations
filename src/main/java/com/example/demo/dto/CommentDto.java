package com.example.demo.dto;

import com.example.demo.entity.Comment;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CommentDto {

    private long id;

    private String content;

    private Timestamp timestamp;

    private long author;

    private long parent;

    private BigDecimal index;

    private String pathString;
//When a commentDto comes from the browser, it brings its parent path in parentPathString
    private String parentPathString;

    private int depth=1;

    private List<CommentDto> children = new ArrayList<>();;

    public CommentDto(Comment comment){
        this.id = comment.getId();
        this.content = comment.getContent();
        this.timestamp = comment.getTimestamp();
        this.author = comment.getAuthor();
        this.index = comment.getIndex();
        this.pathString = comment.getPathString();
        this.depth = comment.getDepth();
        this.parent = comment.getParent();
        //When the comment is converted to the commentDto, there is no need to pass parentPathString
    }


}
