package com.example.demo;

import com.example.demo.dto.CommentDto;
import com.example.demo.entity.Comment;
import com.example.demo.repositories.CommentRepository;
import com.example.demo.services.CommentCRUDService;
import com.example.demo.services.CommentFilterService;
import com.example.demo.services.CommentIndexService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = CrudApplication.class)
public class CRUDoperationsTest {
    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private CommentCRUDService cts;

    @Autowired
    private CommentFilterService filterService;

    @Autowired
    private CommentIndexService indexService;


    int l1Size = 500;
    int l2Depth =100;
    Comment r1;
    List<Comment> l1;
    List<List<Comment>> L1;
    @BeforeEach
    public void setup() {
        commentRepository.deleteAll();
        commentRepository.flush();
        r1 = buildComment("r1 ", 1);

        persistEntity(cts, null, r1);
        l1 = buildAndSaveLvl1(r1,  l1Size,"r1 ");
        PinTreeBuilder ptb = new PinTreeBuilder();
        L1 = new ArrayList<>();
        for (int i = 0; i < l1.size(); i++) {
            List<Comment> temp = ptb.produceList(l1.get(i), l2Depth, "r1 ");
            L1.add(temp);
        }

    }

    @AfterEach
    public void closeUp(){

        commentRepository.deleteAll();
        commentRepository.flush();

    }
    @Test
    public void readTest(){
        int numberOfRuns=10;
        for(int depth =10;depth<=90;depth=depth+10) {

          long  start = System.currentTimeMillis();
            for(int j=0;j<numberOfRuns;j++) {
                for (List<Comment> el : L1) {
                    testSearchIndex(el.get(depth), depth);
                }
            }
          long  end = System.currentTimeMillis();
         long   diff = (end - start);
            System.out.println("runtime index for depth:" + depth + ", the diff is " + diff);
        }

    }
    @Test
    public void deleteTest(){
        Comment c = l1.get(10);
        //First, we check that c exists and has the right number of descendants
        CommentDto result = cts.findCommentDto(c);
        assertEquals(filterService.giveTotalSize(result),l2Depth+1);
        //Then we delete c
         cts.deleteComment(c);
         //Finally, we find c again and check that it is no longer in the DB
         result = cts.findCommentDto(c);
        assertEquals(result,null);
        assertEquals(filterService.giveTotalSize(result),0);



    }
    @Test
    public void findAllRootTest(){
        Collection<Comment> result = cts.findAllRootComments();
        assertEquals(result.size(),1);



    }

    public void testSearchIndex(Comment com, long ind){
           CommentDto result = cts.findCommentDto(com);
          assertEquals(filterService.giveTotalSize(result), l2Depth-ind-1L);

    }
    void persistEntity(CommentCRUDService cts, Comment parent, Comment child){
        if(parent!=null || child.getDepth()>1 ) {
            child.setParent(parent.getId());
            cts.saveComment(child, parent.getPathString());
        }
        else{
            cts.saveRoot(child);
        }
    }
    public Comment buildComment(String content, int depth){
        Comment temp;
        temp= Comment.builder()
                .content(content)
                .author(4)
                .timestamp(null)
                .build();
        temp.setDepth(depth);
        return temp;
    }
    public List<Comment> buildAndSaveLvl1(Comment parent, int l1size, String content){
        List<Comment> l1 = new ArrayList<>();
        Comment temp;
        for(int i=0;i<l1Size;i++){
            temp = buildComment(content+" "+i, 2);

            persistEntity(cts, parent, temp);
            l1.add(temp);
        }
        return l1;
    }
    public class PinTreeBuilder implements HeavyLoadTest.listProducer {
        public List<Comment> produceList(Comment parent, int depth, String content) {
            int intDepth = 3;
            List<Comment> l1 = new ArrayList<>();
            Comment temp;
            temp = buildComment(content + " " + 0, intDepth);
            persistEntity(cts, parent, temp);
            l1.add(temp);
            temp = buildComment(content + " " + 1, intDepth);
            persistEntity(cts, parent, temp);
            l1.add(temp);
            intDepth++;

            for (int i = 2; i < depth; i = i + 2) {
                temp = buildComment(content + " " + i, intDepth);
                persistEntity(cts, l1.get(i - 2), temp);
                l1.add(temp);
                temp = buildComment(content + " " + i + 1, intDepth);
                persistEntity(cts, l1.get(i - 2), temp);
                l1.add(temp);
                intDepth++;
            }
            return l1;
        }
    }
}
