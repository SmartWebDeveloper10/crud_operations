package com.example.demo;

import com.example.demo.dto.CommentDto;
import com.example.demo.entity.Comment;

import com.example.demo.services.impl.CommentFilterServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class CommentFilterServiceTest {
    @InjectMocks
    private CommentFilterServiceImpl commentService;

    Comment c1;
    Comment c2;
    Comment c3;
    Comment c4;
    Comment c5;
    Comment c6;
    Comment c7;
    Comment c8;
    Comment c9;
    Comment c10;
    Comment c11;
    Comment c12;
    Comment c13;
    Comment c14;
    Comment c15;
    Comment c16;
    Comment c17;
    Comment c18;
    Comment c19;
    Comment c20;
    Comment c21;
    Comment c22;
    Comment c23;
    Comment c24;
    Comment c25;
    Comment c26;
    Comment c27;
    Comment c28;
    Comment c29;
    Comment c30;
    Comment c31;
    Comment c32;
    Comment c33;
    Comment cN1;
    Comment cN2;


    Collection<Comment> collection;

    @BeforeEach
    public void setUp() {
        c1 = Comment.builder()
                .id(1L)
                .content("10")
                .author(4L)
                .timestamp(null)
                .depth(1)
                .build();
        c2 = Comment.builder()
                .id(2L)
                .content("20")
                .author(4L)
                .timestamp(null)
                .depth(1)
                .build();
        c3 = Comment.builder()
                .id(3L)
                .content("30")
                .author(4L)
                .timestamp(null)
                .depth(1)
                .build();
        c4 = Comment.builder()
                .id(4L)
                .content("40")
                .author(4L)
                .timestamp(null)
                .depth(2)
                .parent(1)
                .build();
        c5 = Comment.builder()
                .id(5L)
                .content("50")
                .author(4L)
                .timestamp(null)
                .depth(2)
                .parent(1)
                .build();
        c6 = Comment.builder()
                .id(6L)
                .content("60")
                .author(4L)
                .timestamp(null)
                .depth(2)
                .parent(2)
                .build();
        c7 = Comment.builder()
                .id(7L)
                .content("70")
                .author(4L)
                .timestamp(null)
                .depth(2)
                .parent(3)
                .build();
        c8 = Comment.builder()
                .id(8L)
                .content("80")
                .author(4L)
                .timestamp(null)
                .depth(2)
                .parent(3)
                .build();
        c9 = Comment.builder()
                .id(9L)
                .content("90")
                .author(4L)
                .timestamp(null)
                .depth(3)
                .parent(4)
                .build();
        c10 = Comment.builder()
                .id(10L)
                .content("100")
                .author(4L)
                .timestamp(null)
                .depth(3)
                .parent(4)
                .build();
        c11 = Comment.builder()
                .id(11L)
                .content("110")
                .author(4L)
                .timestamp(null)
                .depth(3)
                .parent(4)
                .build();
        c12 = Comment.builder()
                .id(12L)
                .content("120")
                .author(4L)
                .timestamp(null)
                .depth(3)
                .parent(7)
                .build();
        c13 = Comment.builder()
                .id(13L)
                .content("130")
                .author(4L)
                .timestamp(null)
                .depth(4)
                .parent(9)
                .build();
        c14 = Comment.builder()
                .id(14L)
                .content("140")
                .author(4L)
                .timestamp(null)
                .depth(4)
                .parent(9)
                .build();
        c15 = Comment.builder()
                .id(15L)
                .content("150")
                .author(4L)
                .timestamp(null)
                .depth(5)
                .parent(13)
                .build();
        c16 = Comment.builder()
                .id(16L)
                .content("160")
                .author(4L)
                .timestamp(null)
                .depth(6)
                .parent(15)
                .build();
        c17 = Comment.builder()
                .id(17L)
                .content("170")
                .author(4L)
                .timestamp(null)
                .depth(7)
                .parent(16)
                .build();
        c18 = Comment.builder()
                .id(18L)
                .content("180")
                .author(4L)
                .timestamp(null)
                .depth(8)
                .parent(17)
                .build();
        c19 = Comment.builder()
                .id(19L)
                .content("190")
                .author(4L)
                .timestamp(null)
                .depth(8)
                .parent(17)
                .build();
        c20 = Comment.builder()
                .id(20L)
                .content("200")
                .author(4L)
                .timestamp(null)
                .depth(3)
                .parent(5)
                .build();

        c21 = Comment.builder()
                .id(21L)
                .content("210")
                .author(4L)
                .timestamp(null)
                .depth(3)
                .parent(5)
                .build();
        c22 = Comment.builder()
                .id(22L)
                .content("220")
                .author(4L)
                .timestamp(null)
                .depth(4)
                .parent(20)
                .build();
        c23 = Comment.builder()
                .id(23L)
                .content("230")
                .author(4L)
                .timestamp(null)
                .depth(4)
                .parent(20)
                .build();

        c24 = Comment.builder()
                .id(24L)
                .content("240")
                .author(4L)
                .timestamp(null)
                .depth(5)
                .parent(22)
                .build();
        c25 = Comment.builder()
                .id(25L)
                .content("250")
                .author(4L)
                .timestamp(null)
                .depth(6)
                .parent(24)
                .build();
        c26 = Comment.builder()
                .id(26L)
                .content("260")
                .author(4L)
                .timestamp(null)
                .depth(7)
                .parent(25)
                .build();
        c27 = Comment.builder()
                .id(27L)
                .content("270")
                .author(4L)
                .timestamp(null)
                .depth(8)
                .parent(26)
                .build();
        c28 = Comment.builder()
                .id(28L)
                .content("280")
                .author(4L)
                .timestamp(null)
                .depth(8)
                .parent(26)
                .build();
        c29 = Comment.builder()
                .id(29L)
                .content("290")
                .author(4L)
                .timestamp(null)
                .depth(9)
                .parent(27)
                .build();
        c30 = Comment.builder()
                .id(30L)
                .content("300")
                .author(4L)
                .timestamp(null)
                .depth(9)
                .parent(27)
                .build();
        c31 = Comment.builder()
                .id(31L)
                .content("310")
                .author(4L)
                .timestamp(null)
                .depth(8)
                .parent(26)
                .build();
        c32 = Comment.builder()
                .id(32L)
                .content("320")
                .author(4L)
                .timestamp(null)
                .depth(9)
                .parent(31)
                .build();
        c33 = Comment.builder()
                .id(33L)
                .content("330")
                .author(4L)
                .timestamp(null)
                .depth(9)
                .parent(31)
                .build();

        cN1 = Comment.builder()
                .id(101L)
                .content("N1")
                .author(4L)
                .timestamp(null)
                .depth(2)
                .build();
        cN2 = Comment.builder()
                .id(102L)
                .content("N2")
                .author(4L)
                .timestamp(null)
                .depth(2)
                .build();


        collection = new ArrayList<>();

        collection.add(c33);
        collection.add(c29);
        collection.add(c30);
        collection.add(c21);
        collection.add(c22);
        collection.add(c23);
        collection.add(c11);
        collection.add(c12);
        collection.add(c3);
        collection.add(c14);
        collection.add(c5);
        collection.add(c9);
        collection.add(c7);
        collection.add(c8);
        collection.add(c6);
        collection.add(c10);
        collection.add(c1);
        collection.add(c2);
        collection.add(c13);
        collection.add(c4);
        collection.add(c15);
        collection.add(c19);
        collection.add(c16);
        collection.add(c17);
        collection.add(c18);
        collection.add(c20);
        collection.add(c27);
        collection.add(c25);
        collection.add(c24);
        collection.add(c26);
        collection.add(c32);
        collection.add(c31);
        collection.add(c28);
    }

    /*
    If the input collection is not ordered, we have to parse the result tree by hand.
    Otherwise, the input collection is sorted differently for different runs
    (rows with the same depth are placed in different order) and
    the children list items are sequenced differently for every run.
     */
    @Test
    public void correctlyFiltersNestedCommentsFromSubroot() {
        CommentDto res = commentService.filterComments(collection,c4);
        List<CommentDto> children4 = res.getChildren();
        List<CommentDto> children9;
        List<CommentDto> children17;
        for(CommentDto c: children4){
            if(c.getChildren().size()==2){
                assertEquals(c.getId(), 9);
                children9 = c.getChildren();
                for(CommentDto c1:children9){
                    if(c1.getChildren().size()==1){
                        assertEquals(c1.getId(), 13);
                        assertEquals(c1.getChildren().get(0).getId(), 15);
                        assertEquals(c1.getChildren().get(0).getChildren().get(0).getChildren().get(0).getId(), 17);
                        children17 = c1.getChildren().get(0).getChildren().get(0).getChildren().get(0).getChildren();
                        assertEquals(children17.size(), 2);

                        boolean res18 = children17.get(0).getId() == 18 || children17.get(1).getId()==18;
                        assertEquals(res18, true);
                        boolean res19 = children17.get(0).getId() == 19 || children17.get(1).getId()==19;
                        assertEquals(res19, true);




                    }
                    else if(c1.getChildren().size()==0){
                        assertEquals(c1.getId(), 14);
                    }
                }

            }
        }

        assertThat(children4, hasSize(3));

    }
    @Test
    public void correctlyFiltersNestedCommentsFromRoot(){
        CommentDto res;

        res = commentService.filterComments(collection,c7);
        List<CommentDto> children7 = res.getChildren();
        assertEquals(children7.size(), 1);
        assertEquals(children7.get(0).getId(), 12);

        res = commentService.filterComments(collection,c5);
        assertEquals(commentService.giveTotalSize(res), 15);
        List<CommentDto> children5 = res.getChildren();
        List<CommentDto> children20;
        List<CommentDto> children26;
        List<CommentDto> children27;
        List<CommentDto> children31;
        assertEquals(children5.size(), 2);
        for(CommentDto c: children5){
            if(c.getChildren().size()==2){
                assertEquals(c.getId(), 20);
                children20 = c.getChildren();
                for(CommentDto c1:children20){
                    if(c1.getChildren().size()==1){
                        assertEquals(c1.getId(), 22);
                        assertEquals(c1.getChildren().get(0).getId(), 24);
                        assertEquals(c1.getChildren().get(0).getChildren().get(0).getChildren().get(0).getId(), 26);
                        assertEquals(commentService.giveTotalSize(c1.getChildren().get(0).getChildren().get(0).getChildren().get(0)), 8);
                        assertEquals(c1.getChildren().get(0).getChildren().get(0).getChildren().get(0).getChildren().size(), 3);
                        children26 = c1.getChildren().get(0).getChildren().get(0).getChildren().get(0).getChildren();

                        for(CommentDto c3:children26){
                            if(c3.getChildren().size()==0){
                                assertEquals(c3.getId(), 28);
                            }
                            if(c3.getChildren().size()==2 && (c3.getChildren().get(0).getId()==27 || c3.getChildren().get(1).getId()==27)){
                                children27 = c3.getChildren();

                                assertEquals(children27.size(), 2);
                                boolean res27 = children27.get(0).getId() == 29 || children27.get(1).getId()==30;
                                assertEquals(res27, true);


                            }
                            if(c3.getChildren().size()==2 && (c3.getChildren().get(0).getId()==31 || c3.getChildren().get(1).getId()==31)){
                                children31 = c3.getChildren();

                                assertEquals(children31.size(), 2);
                                boolean res31 = children31.get(0).getId() == 31 || children31.get(1).getId()==31;
                                assertEquals(res31, true);
                                boolean res32 = children31.get(0).getId() == 32 || children31.get(1).getId()==32;
                                assertEquals(res32, true);

                            }
                        }
//                        children17 = c1.getChildren().get(0).getChildren().get(0).getChildren().get(0).getChildren();
//                        assertEquals(children17.size(), 2);

                    }
                    else if(c1.getChildren().size()==0){
                        assertEquals(c1.getId(), 23);
                    }
                }

            }
        }

        res = commentService.filterComments(collection,c15);
        List<CommentDto> children15 = res.getChildren();
        assertEquals(children15.size(), 1);

        res = commentService.filterComments(collection,c2);
        List<CommentDto> children2 = res.getChildren();
        assertEquals(children2.size(), 1);
        assertEquals(children2.get(0).getId(), 6);
    }

    @Test
    public void correctlyFiltersNestedCommentsForDelete(){
       List<Comment> res = commentService.filterCommentsForDelete(collection,c7);
        assertEquals(res.size(), 2);
        res = commentService.filterCommentsForDelete(collection,c5);
        assertEquals(res.size(), 15);
        res = commentService.filterCommentsForDelete(collection,c1);
        assertEquals(res.size(), 27);
        res = commentService.filterCommentsForDelete(collection,cN1);
        assertEquals(res.size(), 0);
        res = commentService.filterCommentsForDelete(collection,cN2);
        assertEquals(res.size(), 0);


    }
}
